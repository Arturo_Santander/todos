var todoOperations = function () {
    // variable que tendra en front nuestra listya de todos
    var todos;
    // metodo para pedir al back la lista completa de todos
    function getAllTodos() {
        return $.get("/api/todos", function (data) {
            todos = data;
        });
    }
 // metodo para enviar al back un nuevo todo
    function addTodo(todoItem) {
        return $.post('/api/todos', todoItem);
    }
    // devolvemos un objeto con los metodos publicos que exponemos
    return {
        getAllTodos: getAllTodos,
        addTodo: addTodo
    }
}();

 // variable global para el objeto dom (ul) de la lista de todos
var list;

 // helper para poner en el ul todos los todos
function refreshTodos() {
  // llamamos al servidor y recuperamos la lista 
    todoOperations.getAllTodos().then(function (data) {
      // el server nos devuelve una lista(data) y la recorremos 
        $.each(data, function () {
          // para cada todo añadimos al ul un nuevo li 
            list.append("<li data-id='" + this.id + "'>" + this.text + "</li>");
        });
    });
}

function addTodo () {
      // recupero lo que el user ha puesto en el input 
        var textBox = $("#txtNewTodo");
        // verifico que han metido algo antes de hacer nada
        if (textBox.val() !== "") {
          // llamao a la funcion que guarda el todo
            todoOperations.addTodo({ "text": textBox.val() }).then(function (data) {
                // si todo ha ido bien 
                if (data.success === true) {
                  // añado el todo a la lista 
                    list.append("<li data-id='" + data.item.id + "'>" + data.item.text + "</li>");
                    // vacio el input para que  metan otro 
                    textBox.val('');
                }
                // falta hacer algo cuando error
            });
        }
    }
 // trick de jquery para esperar a que la pagina este cargada 
$(function () {
    // me guardo una referencia al ul de los todos en la variable global 
    list = $("#todoList");
    // bindeo el evento de control de click al boton de añadir todos 
    $("#btnAddTodo").click(addTodo);

 // cargo los datos iniciales de la lista de todos que envia el server 
    refreshTodos();
});